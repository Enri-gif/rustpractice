fn shallow_copy_string()
{
    //let s1 = String::from("hello");
    //let s2 = s1;
    //println!("{s1}");
}

fn deep_copy_string()
{
    let s1 = String::from("hello");
    let _s2 = s1.clone();
    println!("{s1}");
}

fn shallow_copy_int()
{
    let x = 5;
    let _y = x;
    println!("{x}");
}

fn main()
{
    deep_copy_string();
}
