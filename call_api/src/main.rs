use std::collections::HashMap;
use reqwest;
use serde::Deserialize;
use std::error::Error;
use std::time::Duration;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>>
{
    let api_response = get_current_user().unwrap().await;
    println!("{:?}", api_response);
    
    Ok(())
}

async fn get_current_user() -> Result<String, Box<dyn Error>>
{
    let client = reqwest::Client::new();
    let response = client
        .get("https://httpbin.org/get")
        .header("Accept", "text/plain")
        .timeout(Duration::from_secs(3))
        .send()
        .await?
        .text()
        .await?;
    Ok(response)
}