fn main()
{
    // initilisere ny int32 vector med Vec::new() funktionen
    let mut v: Vec<i32> = Vec::new();
    // initilisere en ny vector med en macro
    let v1 = vec![1,2,3];

    // tilføj element til vector
    v.push(1);
    v.push(9);

    // hent værdien med indexering
    let second: &i32 = &v[1];
    println!("v[1] = {second}");

    // hent værdien med get metoden
    let hundred: Option<&i32> = v1.get(100);
    match hundred
    {
        Some(hundred) => println!("v1[100] = {hundred}"),
        None => println!("v1[100] eksistere ikke"),
    }
}
