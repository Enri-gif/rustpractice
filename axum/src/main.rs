use axum::{
    routing::{get, post},
    Router,
};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};
use tower_http::cors::{AllowOrigin, CorsLayer};
use http::{
    Method,
    header::{CONTENT_TYPE}
};

#[tokio::main]
async fn main() {
    // initialize tracing
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(
            std::env::var("RUST_LOG").unwrap_or_else(|_| "axum=debug".into()),
        ))
        .with(tracing_subscriber::fmt::layer())
        .init();
    
    // CORS policy
    let origins = [
        "127.0.0.1".parse().unwrap(),
    ];

    let cors = CorsLayer::new()
        //.allow_methods([Method::GET, Method::POST]) 
        .allow_origin(origins);
        //.allow_headers([CONTENT_TYPE]); 
    
    // print hello world to root route
    let app = Router::new()
        .route("/", get(|| async {"Hello world"}))
        // add CORS layer to serer
        .layer(cors);

    // bind to port 3000 on localhost
    let address = std::net::SocketAddr::from(([127, 0, 0, 1], 3000));
    tracing::debug!("listening on {}", address);

    axum::Server::bind(&address)
        .serve(app.into_make_service())
        .await
        .expect("failed to start server");
}
