fn main()
{
    let mut s1 = String::from("hello");
    //let len = calculate_length(&s1);
    //change_string(&mut s1);
    //println!("{s1} = {len}");

    //let reference_to_nothing = dangling_pointer();
    let _r = no_dangle();
    println!("{_r}");
}

fn no_dangle() -> String
{
    let s = String::from("hello");
    s
}
/*
fn dangling_pointer() -> &String
{
    let s = String::from("hello");
    &s
}
*/
fn second_mutable_borrow()
{
    let mut s1 = String::from("hello");
    let _r1 = &mut s1;
    let _r2 = &mut s1;
    //println!("{_r1} {r2}");
}

fn change_string(s: &mut String)
{
    s.push_str(", world");
}

fn calculate_length(s: &String) -> usize
{
    s.len()
}
